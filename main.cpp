#include <iostream>
#include "vectorTemp.h"
using namespace std;

int main()
{
    //create string vector
    cout << "Creating string vector..." << endl;
    vectorTemp<string> vectorTemp1;
    vectorTemp1.pushback("obj 1");
    vectorTemp1.pushback("obj 2");

    //Aspects
    cout << "Aspects of vector: " << endl;
    //operator overload <<
    cout << "String vector: " << vectorTemp1 << endl;
    //get size
    cout << "Size of string vector: " << vectorTemp1.getSize() << endl;
    //get capacity
    cout << "Capacity of string vector: " << vectorTemp1.getCapacity() << endl;
    cout << endl;

    //make string vector go past capacity
    cout << "Making string vector grow past capacity..." << endl;
    for (int i = 0; i < 10; i++)
    {
        vectorTemp1.pushback("obj");
    }
    //Aspects
    cout << "Aspects of vector (2): " << endl;
    //operator overload <<
    cout << "String vector: " << vectorTemp1 << endl;
    //get size
    cout << "Size of string vector: " << vectorTemp1.getSize() << endl;
    //get capacity
    cout << "Capacity of string vector: " << vectorTemp1.getCapacity() << endl;
    cout << endl;


    //make string vector shrink
    cout << "Making string vector shrink..." << endl;
    for (int i = 0; i < 9; i++)
    {
        vectorTemp1.popback();
    }
    //Aspects
    cout << "Aspects of vector (3): " << endl;
    //operator overload <<
    cout << "String vector: " << vectorTemp1 << endl;
    //get size
    cout << "Size of string vector: " << vectorTemp1.getSize() << endl;
    //get capacity
    cout << "Capacity of string vector: " << vectorTemp1.getCapacity() << endl;
    cout << endl;

    //show indexing
    cout << "Indexing..." << endl;
    string k = vectorTemp1[0];
    cout << "The first element in string vector is: " << k << endl;
    cout << endl;

    //show copy constructor
    vectorTemp<string> vectorCopy1 = vectorTemp1;
    cout << "Copy function of vector (copy of string vector): " << vectorCopy1 << endl;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //create int vector
    cout << "\n\n\n\n\n\n\nCreating int vector..." << endl;
    vectorTemp<int> vectorTemp2;
    vectorTemp2.pushback(1);
    vectorTemp2.pushback(2);

    //Aspects
    cout << "Aspects of vector: " << endl;
    //operator overload <<
    cout << "Int vector: " << vectorTemp2 << endl;
    //get size
    cout << "Size of int vector: " << vectorTemp2.getSize() << endl;
    //get capacity
    cout << "Capacity of int vector: " << vectorTemp2.getCapacity() << endl;
    cout << endl;

    //make string vector go past capacity
    cout << "Making int vector grow past capacity..." << endl;
    for (int i = 3; i < 13; i++)
    {
        vectorTemp2.pushback(i);
    }
    //Aspects
    cout << "Aspects of vector (2): " << endl;
    //operator overload <<
    cout << "Int vector: " << vectorTemp2 << endl;
    //get size
    cout << "Size of int vector: " << vectorTemp2.getSize() << endl;
    //get capacity
    cout << "Capacity of int vector: " << vectorTemp2.getCapacity() << endl;
    cout << endl;


    //make string vector shrink
    cout << "Making int vector shrink..." << endl;
    for (int i = 0; i < 9; i++)
    {
        vectorTemp2.popback();
    }
    //Aspects
    cout << "Aspects of vector (3): " << endl;
    //operator overload <<
    cout << "Int vector: " << vectorTemp2 << endl;
    //get size
    cout << "Size of int vector: " << vectorTemp2.getSize() << endl;
    //get capacity
    cout << "Capacity of int vector: " << vectorTemp2.getCapacity() << endl;
    cout << endl;

    //show indexing
    cout << "Indexing..." << endl;
    int l = vectorTemp2[0];
    cout << "The first element in int vector is: " << l << endl;
    cout << endl;

    //show copy constructor
    vectorTemp<int> vectorCopy2 = vectorTemp2;
    cout << "Copy function of vector (copy of int vector): " << vectorCopy2 << endl;
    return 0;
}