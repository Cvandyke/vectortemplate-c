This program includes a template for the vector class in c++.
This program allows the ability to add items (pushback),
get items (popback or [index]), copy a vector to another,
get the capacity, get the size, and print the vector.  

