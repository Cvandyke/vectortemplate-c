//
// Created by Chandler on 10/12/2016.
//

#ifndef VECTORTEMP_VECTORTEMP_H
#define VECTORTEMP_VECTORTEMP_H

#include <iostream>
#include <array>
#include <string>

using namespace std;

template<typename t>

class vectorTemp{


private:
    int size;
    int capacity;
    t* storage;

    void growVector()
    {
        //increase capacity
        capacity += 10;
        //create new array instance
        t* newStorage = new t[capacity];
        //iterate through to copy old to new
        for (int index = 0; index < size; index++ )
        {
            newStorage[index] = storage[index];
        }
        //delete old storage
        delete[] storage;
        //make old = new
        storage = newStorage;
        return;
    }


    void shrinkVector()
    {
        //decrease capacity by 10
        capacity -= 10;
        //create new array instance
        t* newStorage = new t[capacity];
        //iterate through to copy old to new
        for (int index = 0; index < size; index++)
        {
            newStorage[index] = storage[index];
        }
        //delete old storage
        delete[] storage;
        //make old = new
        storage = newStorage;
        return;
    }

    //get storage
    t *getStorage() const {
        return storage;
    }


public:
    //defualt constructor
    vectorTemp()
    {
        //initialize size
        size = 0;
        //initialize capacity
        capacity = 10;
        //initialize storage
        storage = new t [10];
    }

    //default destructor
    ~vectorTemp()
    {
        //clean up
        delete storage;
    }


    //get capacity
    int getCapacity ()
    {
        //return capacity
        return capacity;
    }

    //get number of elements
    int getSize ()
    {
        //return size
        return size;
    }

    //copy constructor
    vectorTemp (vectorTemp<t>& origVector)
    {
        //copy storage over
        storage = origVector.getStorage();
        //copy size
        size = origVector.getSize();
        //copy capacity
        capacity = origVector.getCapacity();

    }

    //pushback
    void pushback(t element)
    {
        //if size == capacity
        if (size == capacity)
        {
            //grow vector
            growVector();
        }

        //add element to storage
        storage[size] = element;
        size += 1;

        return;
    }

    //popback
    t popback()
    {
        //create instance of element
        t element;
        //if storage is not empty
        if (size > 0)
        {
            //assign element to last element in storage
            size -= 1;
            element = storage[size];
        }
        //shrink array if needed (size is 10 less than capacity and capacity is not already 10)
        if (size + 10 == capacity and capacity != 10)
        {
            shrinkVector();
        }
        return element;
    }

    //get value
    t getValue(int index)
    {
        return storage[index];
    }

    //overload operators

    //overload []
    t &operator[](int index)
    {
        if(index > size)
        {
            cout << "Index out of bounds" << endl;
            //return the first element
            return storage[0];
        }
        return storage[index];
    }

    //overload <<
    friend ostream& operator<<(ostream& output, const vectorTemp &temp)
    {
        //create a string of storage in an array formatted to "[x, x, x....]"
        string outString;
        output << "[";
        output << temp.storage[0];
        //add other elements if there are more
        if (temp.size > 1)
            for (int index = 1; index < temp.size; index++)
            {
                output << ", " << temp.storage[index];
            }
        //add closing bracket
        output << "]";
        //finish

        return output;
    }
};


#endif //VECTORTEMP_VECTORTEMP_H